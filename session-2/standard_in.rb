def colorize(text, color_code)
  "\33[#{color_code}m#{text}\33[0m"
end

while line = $stdin.gets
  print colorize(line, 33)
  puts colorize('Done printing line from standard input', 35)
end
