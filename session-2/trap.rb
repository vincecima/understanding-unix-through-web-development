Signal.trap('INT') do
  puts "Caught INT, exiting!"
  exit(1)
end

own_pid = Process.pid
puts "I am process #{own_pid}"

puts "Sending myself the INT signal"
Process.kill('INT', own_pid)
sleep(5)

puts 'How did we get here?'
