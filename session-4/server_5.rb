require 'socket'

PORT_NUMBER = 32400

puts "I am PID #{Process.pid}"

socket = Socket.new(:INET, :STREAM)
puts 'Socket created'

socket.bind(Addrinfo.tcp('127.0.0.1', PORT_NUMBER))

CONNECTION_BACKLOG_SIZE = 100
socket.listen(CONNECTION_BACKLOG_SIZE)

loop do
  puts "Accepting the next connection"
  client, addr_info = socket.accept
  puts "Received connection from #{client.inspect}"

  puts "Sending the current timestamp"
  # Socket inherits from IO in Ruby
  # Exposes a puts method (just like a file) that lets me write text to the network stream
  client.puts Time.now

  puts "Ending connection"
  client.close
end
