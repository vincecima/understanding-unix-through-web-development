require 'socket'

COLORS = [31,32,33,34,35,36,37]
def colorize(text, color_code)
  "\33[#{color_code}m#{text}\33[0m"
end

PORT_NUMBER = 32400

srand

puts "#{Process.pid}: I am the parent client"

10.times do
  # Block only executes in the child process
  fork do
    color_code = COLORS.sample

    socket = Socket.new(:INET, :STREAM)
    socket.connect(Addrinfo.tcp('127.0.0.1', PORT_NUMBER))
    puts colorize("#{Process.pid}: Connection opened", color_code)

    sleep_time = rand(4)
    puts colorize("#{Process.pid}: Sleeping for #{sleep_time} seconds", color_code)
    sleep(sleep_time)
    # Send data to server
    socket.puts 'PING'

    puts colorize("#{Process.pid}: Waiting for server", color_code)
    # Wait for server to send data back
    server_output = socket.readline
    puts colorize("#{Process.pid}: Server said #{server_output}", color_code)
    socket.close
  end
end


puts "#{Process.pid}: Waiting for clients to finish"
sleep(60)
puts "#{Process.pid}: Exiting"
