require 'socket'

COLORS = [31,32,33,34,35,36,37]
def colorize(text, color_code)
  "\33[#{color_code}m#{text}\33[0m"
end

PORT_NUMBER = 32400

srand

puts "#{Process.pid}: I am the parent server"

socket = Socket.new(:INET, :STREAM)
socket.bind(Addrinfo.tcp('127.0.0.1', PORT_NUMBER))
CONNECTION_BACKLOG_SIZE = 100
socket.listen(CONNECTION_BACKLOG_SIZE)

puts "#{Process.pid}: Listening for connections on #{PORT_NUMBER}"

loop do
  client, addr_info = socket.accept
  puts "#{Process.pid}: Connection accepted from #{client.inspect}, #{addr_info.inspect}"

  # Block only executes in the child process
  fork do
    color_code = COLORS.sample

    puts colorize("#{Process.pid}: Reading output from client", color_code)
    # Wait until the client sends some data
    client_output = client.readline
    puts colorize("#{Process.pid}: Client said #{client_output}", color_code)

    sleep_time = rand(4)
    puts colorize("#{Process.pid}: Sleeping for #{sleep_time} seconds", color_code)
    sleep(sleep_time)

    puts colorize("#{Process.pid}: Replying to client", color_code)
    client.puts 'PONG'
  end
end
