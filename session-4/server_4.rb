require 'socket'

PORT_NUMBER = 32400

puts "I am PID #{Process.pid}"

socket = Socket.new(:INET, :STREAM)
puts 'Socket created'

socket.bind(Addrinfo.tcp('127.0.0.1', PORT_NUMBER))

CONNECTION_BACKLOG_SIZE = 100
socket.listen(CONNECTION_BACKLOG_SIZE)

puts "Accepting the next connection"
# Accept the next connection requested by a client
client, addr_info = socket.accept
# At this point we can send or receive data as needed
puts "Received connection from #{client.inspect} #{addr_info.inspect}"

gets
socket.close
puts "Socket closed"
exit
