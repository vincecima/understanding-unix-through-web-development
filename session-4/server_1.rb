require 'socket'

# Print PID
puts "I am PID #{Process.pid}"

# Try to create a socket for TCP IPv4 in Stream mode
socket = Socket.new(:INET, :STREAM)
puts "Socket created"
puts "Press Enter to exit"
gets
# Socket's should be closed (just as you would close a file)
socket.close
puts "Socket closed"
exit
