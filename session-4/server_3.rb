require 'socket'

PORT_NUMBER = 32400

puts "I am PID #{Process.pid}"

socket = Socket.new(:INET, :STREAM)
puts 'Socket created'

socket.bind(Addrinfo.tcp('127.0.0.1', PORT_NUMBER))

# Socket is now in the listening state and is ready to accept connections
# CONNECTION_BACKLOG_SIZE - maximum number of connections the OS will allow to wait for this socket
CONNECTION_BACKLOG_SIZE = 100
socket.listen(CONNECTION_BACKLOG_SIZE)

gets
socket.close
puts "Socket closed"
exit
