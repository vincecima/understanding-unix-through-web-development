require 'socket'

PORT_NUMBER = 32400

puts "I am PID #{Process.pid}"

socket = Socket.new(:INET, :STREAM)
puts 'Socket created'

# Assigns socket to a specific network address and port
# 127.0.0.1 is the loopback address, way for a computer to talk to itself over the network
# PORT_NUMBER lets multiple programs listen for traffic on the same computer + address
socket.bind(Addrinfo.tcp('127.0.0.1', PORT_NUMBER))
puts "Socket bound to port #{PORT_NUMBER}"

gets
socket.close
puts "Socket closed"
exit
