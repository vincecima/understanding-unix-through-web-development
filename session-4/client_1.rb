require 'socket'

PORT_NUMBER = 32400

puts "I am PID #{Process.pid}"

# Try to create a socket for TCP IPv4 in Stream mode
socket = Socket.new(:INET, :STREAM)

# Connect to the remote computer at this address and port
socket.connect(Addrinfo.tcp('127.0.0.1', PORT_NUMBER))

# socket implements IO, so I can use readline once to grab input
server_output = socket.readline

puts "The server said: #{server_output}"

puts "Ending connection"
socket.close
