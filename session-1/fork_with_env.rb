COLORS = [31,32,33,34,35,36,37]
def colorize(text, color_code)
  "\33[#{color_code}m#{text}\33[0m"
end

puts "I am #{Process.pid}"
sleep(2)

Process.fork

color_code = ENV['COLOR_CODE'] || COLORS.sample

puts colorize("I am #{Process.pid}", color_code)
sleep(30)

puts colorize("Goodbye", color_code)
